﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.espertech.esper.core.service;
using com.espertech.esper.client;

namespace NEsperStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            Configuration config = new Configuration();
            // do something about config

            // TODO understand
            //config.EngineDefaults.EventMetaConfig.ClassPropertyResolutionStyle = PropertyResolutionStyle.CASE_INSENSITIVE;

            // create a statemnt
            var epService = EPServiceProviderManager.GetDefaultProvider(config);
            var expression = "select avg(Price) as avg from NEsperStudy.OrderEvent.win:time(30 sec)";
            var statement = epService.EPAdministrator.CreateEPL(expression);
            statement.Events += SampleEvent.SampleEvents;

            while (true)
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                OrderEvent o_event = new OrderEvent("shirt", rand.Next(100));
                epService.EPRuntime.SendEvent(o_event);
            }
        }
    }

    class SampleEvent
    {
        public static void SampleEvents(object sender, UpdateEventArgs e)
        {
            foreach (EventBean @event in e.NewEvents)
            {
                Console.WriteLine("avg is {0}",
                                    @event.Get("avg"));
            }

        }
    }
}
