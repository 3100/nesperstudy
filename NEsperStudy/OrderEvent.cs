﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 以下のチュートリアルをc#で
 * http://esper.codehaus.org/tutorials/tutorial/quickstart.html
 * 
 */

namespace NEsperStudy
{
    public class OrderEvent
    {
        public string ItemName { get; set; }
        public double Price { get; set; }

        public OrderEvent(string itemName, double price)
        {
            ItemName = itemName;
            Price = price;
        }
    }
}
