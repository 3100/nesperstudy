﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.espertech.esper.client;

namespace NEsperStudy
{
    class MyListener : UpdateListener
    {
        #region UpdateListener メンバー

        public void Update(EventBean[] newEvents, EventBean[] oldEvents)
        {
            var eventbean = newEvents.ElementAt(0);
            Console.WriteLine("avg=" + eventbean.Get("avg(Price)"));
        }

        #endregion
    }
}
